﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Management;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace JBe.Security
{
    /// <summary>
    /// Generates a 16 byte unique identification code of a computer
    /// Example: 4876-8DB5-EE85-69D3-FE52-8CF7-395D-2EA9
    /// </summary>
    public class ComputerFingerprint
    {
        private static ComputerFingerprint @default;

        private String fingerprint;

        private Boolean useMacAddress;
        private Boolean useDiskId;
        private Boolean useVideoCardId;
        private Boolean useBaseId;
        private Boolean useBiosId;
        private Boolean useCpuId;


        /// <summary>
        /// Gets the default intance.
        /// </summary>
        /// <value>
        /// The default instance
        /// </value>
        public static ComputerFingerprint Default
        {
            [DebuggerStepThrough]
            get
            {
                if (@default == null)
                {
                    Interlocked.CompareExchange(ref @default, new ComputerFingerprint(), null);
                }
                return @default;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerFingerprint"/> class.
        /// </summary>
        public ComputerFingerprint()
        {
            useBaseId = true;
            useBiosId = true;
            useCpuId = true;
            useDiskId = true;
            useMacAddress = true;
            useVideoCardId = true;
        }

        public bool UseCpuId
        {
            [DebuggerStepThrough]
            get { return useCpuId; }
            [DebuggerStepThrough]
            set
            {
                useCpuId = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }


        public bool UseBiosId
        {
            [DebuggerStepThrough]
            get { return useBiosId; }
            [DebuggerStepThrough]
            set
            {
                useBiosId = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }

        public bool UseBaseId
        {
            [DebuggerStepThrough]
            get { return useBaseId; }
            [DebuggerStepThrough]
            set
            {
                useBaseId = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }

        public bool UseDiskId
        {
            [DebuggerStepThrough]
            get { return useDiskId; }
            [DebuggerStepThrough]
            set
            {
                useDiskId = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }

        public bool UseVideoCardId
        {
            [DebuggerStepThrough]
            get { return useVideoCardId; }
            [DebuggerStepThrough]
            set
            {
                useVideoCardId = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }

        public bool UseMacAddress
        {
            [DebuggerStepThrough]
            get { return useMacAddress; }
            [DebuggerStepThrough]
            set
            {
                useMacAddress = value;
                Interlocked.Exchange(ref fingerprint, null);
            }
        }

        //// <summary>
        /// Returns a <see cref="System.String" /> with 16 byte unique identification code of a computer.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> with 16 byte unique identification code of a computer.
        /// </returns>
        [DebuggerStepThrough]
        public String Value()
        {
            if (fingerprint == null)
            {
                Interlocked.CompareExchange(ref fingerprint, CreateFingerprint(), null);
            }

            return fingerprint;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> with 16 byte (32 chars) fingerprint of selected hardware.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> with 16 byte (32 chars) fingerprint of selected hardware.
        /// </returns>
        public override string ToString()
        {
            return Value();
        }

        private string CreateFingerprint()
        {
            StringBuilder sb = new StringBuilder();

            if (useCpuId)
                sb.AppendFormat("CPU >> {0}", CpuId());

            if (useBiosId)
                sb.AppendFormat("Bios >> {0}", BiosId());

            if (useBaseId)
                sb.AppendFormat("Base >> {0}", BaseId());

            if (useDiskId)
                sb.AppendFormat("Disk >> {0}", DiskId());

            if (useVideoCardId)
                sb.AppendFormat("Video >> {0}", VideoId());

            if (useMacAddress)
                sb.AppendFormat("Mac >> {0}", MacId());

            if (sb.Length == 0)
                throw new InvalidOperationException("Allow at least one property to collection from.");

            var hash = GetHash(sb.ToString());
            return hash;
        }

        private static string GetHash(string s)
        {
            using (MD5 sec = new MD5CryptoServiceProvider())
            {
                byte[] bytes = Encoding.ASCII.GetBytes(s);
                return GetHexString(sec.ComputeHash(bytes));
            }
        }

        private static string GetHexString(byte[] bytes)
        {
            var sb = new StringBuilder(32);
            for (int i = 0; i < bytes.Length; i++)
            {
                int n, n1, n2;
                byte b = bytes[i];

                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;

                if (n2 > 9)
                {
                    sb.Append(((char)(n2 - 10 + (int)'A')).ToString());
                }
                else
                {
                    sb.Append(n2.ToString());
                }
                if (n1 > 9)
                {
                    sb.Append(((char)(n1 - 10 + (int)'A')).ToString());
                }
                else
                {
                    sb.Append(n1.ToString());
                }
                if ((i + 1) != bytes.Length && (i + 1) % 2 == 0)
                {
                    sb.Append("-");
                }
            }
            return sb.ToString();
        }

        #region Original Device ID Getting Code
        //Return a hardware Identifier
        private static string Identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            ManagementClass mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            object mgmtObj;
            foreach (ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            mgmtObj = mo[wmiProperty];
                            if (mgmtObj != null)
                            {
                                result = mgmtObj.ToString();
                                break;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return result;
        }
        //Return a hardware Identifier
        private static string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            ManagementClass mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            object mgmtObj;
            foreach (ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        mgmtObj = mo[wmiProperty];
                        if (mgmtObj != null)
                        {
                            result = mgmtObj.ToString();
                            break;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        private static string CpuId()
        {
            //Uses first CPU Identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no uniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                }

                //Add clock speed for extra security
                retVal += identifier("Win32_Processor", "MaxClockSpeed");
            }
            return retVal;
        }

        //BIOS Identifier
        private static string BiosId()
        {
            return identifier("Win32_BIOS", "Manufacturer")
            + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
            + identifier("Win32_BIOS", "identificationCode")
            + identifier("Win32_BIOS", "SerialNumber")
            + identifier("Win32_BIOS", "ReleaseDate")
            + identifier("Win32_BIOS", "Version");
        }

        //Main physical hard drive ID
        private static string DiskId()
        {
            return identifier("Win32_DiskDrive", "Model")
            + identifier("Win32_DiskDrive", "Manufacturer")
            + identifier("Win32_DiskDrive", "Signature")
            + identifier("Win32_DiskDrive", "TotalHeads");
        }
        //Motherboard ID
        private static string BaseId()
        {
            return identifier("Win32_BaseBoard", "Model")
            + identifier("Win32_BaseBoard", "Manufacturer")
            + identifier("Win32_BaseBoard", "Name")
            + identifier("Win32_BaseBoard", "SerialNumber");
        }

        //Primary video controller ID
        private static string VideoId()
        {
            return identifier("Win32_VideoController", "DriverVersion")
                + identifier("Win32_VideoController", "Name");
        }

        //First enabled network card ID
        private static string MacId()
        {
            return Identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
        }
        #endregion
    }
}
