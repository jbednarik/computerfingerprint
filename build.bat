@echo off

set fdir=%WINDIR%\Microsoft.NET\Framework64

if not exist %fdir% (
	set fdir=%WINDIR%\Microsoft.NET\Framework
)

set msbuild=%fdir%\v4.0.30319\msbuild.exe

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net40\Debug\AnyCPU
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net40\Debug\x86
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net40\Debug\x64
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"


%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net40\Release\AnyCPU
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net40\Release\x86 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net40\Release\x64 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"


%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net35\Debug\AnyCPU 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net35\Debug\x86 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net35\Debug\x64 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"


%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net35\Release\AnyCPU 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net35\Release\x86 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net35\Release\x64 
FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"


reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NETFramework\v4.0.30319\SKUs\.NETFramework,Version=v4.5" 2>nul
if errorlevel 0 (
  %msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net45\Debug\AnyCPU 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"
	
	%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net45\Debug\x86 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"
	
	%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Debug /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net45\Debug\x64 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"

	
	%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:OutputPath=..\..\..\bin\Net45\Release\AnyCPU 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"
	
	%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x86 /p:OutputPath=..\..\..\bin\Net45\Release\x86 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"
	
	%msbuild% .\solution\ComputerFingerprint-vs2013.sln /m /p:Configuration=Release /t:Clean;Rebuild /p:Platform=x64 /p:OutputPath=..\..\..\bin\Net45\Release\x64 
	FOR /F "tokens=*" %%G IN ('DIR /B /AD /S obj') DO RMDIR /S /Q "%%G"
)

pause